import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 11.04.2017.
 */
public class verifyOrder extends baseClass {

    //Подтверждение, что заказ успешно оформлен

    public verifyOrder(WebDriver driver){this.driver = driver;}

    public String getMessage(){return driver.findElement(showMessage).getText();}
}
