import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 13.04.2017.
 */
public class baseClass {

    public WebDriver driver;

    //add to cart locators

    By addBookToCart = By.xpath(".//*[@id='mm-0']/div[2]/div[5]/div[2]/div/div[2]/div[2]/div[2]/button");
    By buyBook = By.xpath("html/body/div[2]/div/div[1]/div/div[1]/div/footer/div/div[1]/div/div/a/span/span");

    //verify order page locators

    By textInCart = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[2]/div/div/div/div[1]/div/span");

    //step one locators

    By fullName = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[1]/div/div[2]/div/div/div[3]/div/div[1]/div/input");
    By phoneNumber = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[1]/div/div[2]/div/div/div[3]/div/div[2]/div/input");
    By emailAddress = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[1]/div/div[2]/div/div/div[3]/div/div[3]/div/input");
    By stepTwoButton = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[1]/div/div[2]/div/div/div[3]/div/button");

    //step two locators

    By locality = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[2]/div[1]/input[1]");
    By localitySelect = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[2]/div[2]/div[1]");
    By street = By.xpath("html/body/div[1]/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[3]/div/input");
    By houseNumber = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[4]/div/input");
    By housing = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[5]/div/input");
    By appart = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[6]/div/input");
    By comment = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[7]/div/textarea");
    By stepThreeButton = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div/div/div[3]/div/div/div[8]/button");

    //step three locators

    By payOut = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[3]/div/div[2]/div/div/div[2]/div/div[1]/div/label[6]/i");
    By terms = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[3]/div/div[2]/div/div/div[2]/div/div[3]/div/label/ins");
    By orderButton = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[3]/div/div[2]/div/div/div[2]/div/div[4]/button");

    //verify order locators

    By showMessage = By.xpath(".//*[@id='mm-0']/div[2]/div[3]/div/div/div[1]/div[3]/div/div[2]/div/div/div[2]/div/div[3]/div/label/span[2]/span");

}
