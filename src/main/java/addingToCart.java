import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 14.04.2017.
 */
public class addingToCart extends baseClass {

    public addingToCart(WebDriver driver){this.driver = driver;}

    //Работа с кнопками "Купить книгу" и "Оформить заказ"

    public void setAddBookToCart(){driver.findElement(addBookToCart).click();}

    public void setBuyBook(){driver.findElement(buyBook).click();}

    public void goToOrderPage(){
        this.setAddBookToCart();
        this.setBuyBook();
    }

}
