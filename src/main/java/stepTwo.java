import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 11.04.2017.
 */
public class stepTwo extends baseClass{

    public stepTwo(WebDriver driver){this.driver = driver;}

    public void setLocality(){driver.findElement(locality).sendKeys("Москва");}
    public void setLocalitySelect(){driver.findElement(localitySelect).click();}
    public void setStreet(String strStreet){driver.findElement(street).sendKeys(strStreet);}
    public void setHouseNumber(String strHouseNumber){driver.findElement(houseNumber).sendKeys(strHouseNumber);}
    public void setHousing(String strHousing){driver.findElement(housing).sendKeys(strHousing);}
    public void setAppart(String strAppart){driver.findElement(appart).sendKeys(strAppart);}
    public void setComment(String strComment){driver.findElement(comment).sendKeys(strComment);}
    public void setStepThreeButton(){driver.findElement(stepThreeButton).click();}

    public void enterDataStepTwo(String strStreet, String strHouseNumber, String strHousing, String strAppart, String strComment){

        this.setLocality();
        this.setLocalitySelect();
        this.setStreet(strStreet);
        this.setHouseNumber(strHouseNumber);
        this.setHousing(strHousing);
        this.setAppart(strAppart);
        this.setComment(strComment);
        this.setStepThreeButton();
    }
}
