import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 11.04.2017.
 */
public class stepThree extends baseClass {

    public stepThree(WebDriver driver) {this.driver = driver;}

    public void setPayOut(){driver.findElement(payOut).click();}
    public void setTerms(){driver.findElement(terms).click();}
    public void setOrderButton(){driver.findElement(orderButton).click();}

    public void enterDataStepThree(){
        this.setPayOut();
        this.setTerms();
        this.setOrderButton();

    }

}
