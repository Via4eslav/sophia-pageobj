import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 11.04.2017.
 */
public class stepOne extends baseClass {

    public stepOne(WebDriver driver) {this.driver = driver;}

    public void setFullName(String strFullName) {driver.findElement(fullName).sendKeys(strFullName);}
    public void setPhoneNumber() {
        driver.findElement(phoneNumber).click();
        driver.findElement(phoneNumber).sendKeys("8005553535");
    }
    public void setEmailAddress(String strEmailAddress) {driver.findElement(emailAddress).sendKeys(strEmailAddress);}
    public void setStepTwoButton(){driver.findElement(stepTwoButton).click();}

    public void enterDataStepOne(String strFullname, String strEmailAddress) {
        this.setFullName(strFullname);
        this.setPhoneNumber();
        this.setEmailAddress(strEmailAddress);
        this.setStepTwoButton();
    }
}
