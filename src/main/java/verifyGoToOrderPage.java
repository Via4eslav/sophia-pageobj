import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 14.04.2017.
 */
public class verifyGoToOrderPage extends baseClass {

    //Подтверждение, что загрузилась страница оформления товара

    public verifyGoToOrderPage(WebDriver driver){this.driver = driver;}

    public String getTextOnOrderPage(){return driver.findElement(textInCart).getText();}
}
