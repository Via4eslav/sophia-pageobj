import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by ivanov.v on 11.04.2017.
 */
public class sophiaOrder {
    WebDriver driver;
    addingToCart objAddingToCart;
    verifyGoToOrderPage objVerifyGoToOrderPage;
    stepOne objStepOne;
    stepTwo objStepTwo;
    stepThree objStepThree;
    verifyOrder objVerifyOrder;

    @BeforeTest

    //Настраиваем браузер

    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://inkubator.ks.ua/html/sophia/book.html");
    }

    @Test

    //Тестируем оформление заказа

    public  void makingOrder(){
        objAddingToCart = new addingToCart(driver);
        objAddingToCart.goToOrderPage();

        objVerifyGoToOrderPage = new verifyGoToOrderPage(driver);
        Assert.assertTrue(objVerifyGoToOrderPage.getTextOnOrderPage().contains("Состав вашего заказа"));

        objStepOne = new stepOne(driver);
        objStepOne.enterDataStepOne("Иванов Вячеслав",  "ivanov.v.wezom@gmail.com");

        objStepTwo = new stepTwo(driver);
        objStepTwo.enterDataStepTwo("Димитрова", "78", "12", "45","Lorem ipsum, gospoda");

        objStepThree = new stepThree(driver);
        objStepThree.enterDataStepThree();

        objVerifyOrder = new verifyOrder(driver);
        Assert.assertTrue(objVerifyOrder.getMessage().contains("Ваш заказ принят! Спасибо"));

    }

}
